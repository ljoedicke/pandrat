package de.vitox.ratolotl.client.util;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

public class SendUtil {

    private DataInputStream in;
    private DataOutputStream out;

    public static SendUtil instance = new SendUtil();

    public static SendUtil getInstance() {
        return instance;
    }

    public void initIO(Socket socket) throws IOException {
        this.in = new DataInputStream(socket.getInputStream());
        this.out = new DataOutputStream(socket.getOutputStream());
    }

    public void writeMsg(String message, Socket socket) throws IOException {
        out.writeInt(message.getBytes().length);
        out.write(message.getBytes());
        out.flush();
    }

    public void writeBytes(byte[] message, Socket socket) throws IOException {
        out.write(message);
        out.flush();
    }

    public void writeInt(int message, Socket socket) throws IOException {
        out.writeInt(message);
        out.flush();
    }

    public void writeByte(byte message, Socket socket) throws IOException {
        out.writeByte(message);
        out.flush();
    }

    public void write(byte[] messgae, Socket socket) throws IOException {
        out.write(messgae);
        out.flush();
    }

    public void writeShort(short s, Socket socket) {
        try {
            out.writeShort(s);
            out.flush();
        } catch (Exception var3) {
            var3.printStackTrace();
        }

    }

    public String readMsg(Socket socket) throws IOException {
        int length = in.readInt();
        byte[] bytes = new byte[length];
        in.readFully(bytes);
        return new String(bytes, StandardCharsets.US_ASCII);
    }

}
