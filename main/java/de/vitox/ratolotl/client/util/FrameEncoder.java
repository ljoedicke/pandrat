package de.vitox.ratolotl.client.util;

import java.awt.AWTException;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Stroke;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

public final class FrameEncoder {
    private static final int SKIP = 6;
    private static final int TOLERANCE = 50;
    private static final int CELLS_WIDE = 6;
    private static final int CELLS_HIGH = 6;
    private static final Frame[] EMPTY_ARRAY = new Frame[0];
    private static final int CURSOR_SIZE = 6;
    private static final Stroke CURSOR_STROKE = new BasicStroke(2.0f);

    private FrameEncoder() {
    }

    private static boolean isEqual(int rgb1, int rgb2, int tolerance) {
        int red1 = rgb1 >> 16 & 255;
        int green1 = rgb1 >> 8 & 255;
        int blue1 = rgb1 & 255;
        int red2 = rgb1 >> 16 & 255;
        int green2 = rgb2 >> 8 & 255;
        int blue2 = rgb2 & 255;
        int red = Math.abs((int)(red1 - red2));
        int green = Math.abs((int)(green1 - green2));
        int blue = Math.abs((int)(blue1 - blue2));
        if (red <= tolerance && green <= tolerance && blue <= tolerance) {
            return true;
        }
        return false;
    }

    public static BufferedImage takeScreenshot() {
        Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        Rectangle screenRect = new Rectangle(screen);
        try {
            Robot robot = new Robot();
            BufferedImage image = robot.createScreenCapture(screenRect);
            return image;
        }
        catch (AWTException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static BufferedImage captureScreen() {
        BufferedImage image = FrameEncoder.takeScreenshot();
        if (image == null) {
            return null;
        }
        Point mousePoint = MouseInfo.getPointerInfo().getLocation();
        int mouseX = mousePoint.x - 3;
        int mouseY = mousePoint.y - 3;
        Graphics2D g = image.createGraphics();
        ImageUtil.applyHighGraphics((Graphics2D)g);
        g.setStroke(CURSOR_STROKE);
        g.setColor(Color.RED);
        g.drawOval(mouseX, mouseY, 6, 6);
        g.dispose();
        return image;
    }

    public static Frame[] getIFrames(BufferedImage previous, BufferedImage next) {
        int width = previous.getWidth();
        int height = previous.getHeight();
        if (next.getWidth() != width || next.getHeight() != height) {
            return EMPTY_ARRAY;
        }
        int cellWidth = width / 6;
        int cellHeight = height / 6;
        ArrayList frames = new ArrayList();
        int x = 0;
        while (x < 6) {
            int y = 0;
            while (y < 6) {
                int cellX = x * cellWidth;
                int cellY = y * cellHeight;
                int cellEndX = cellX + cellWidth;
                int cellEndY = cellY + cellHeight;
                int xx = cellX;
                block2 : while (xx < cellEndX && xx < width) {
                    int yy = cellY;
                    while (yy < cellEndY && yy < height) {
                        int nextRgb;
                        int previousRgb = previous.getRGB(xx, yy);
                        boolean equal = FrameEncoder.isEqual(previousRgb, nextRgb = next.getRGB(xx, yy), 50);
                        if (!equal) {
                            BufferedImage image = next.getSubimage(cellX, cellY, cellWidth, cellHeight);
                            Frame frame = new Frame(cellX, cellY, image);
                            frames.add((Object)frame);
                            break block2;
                        }
                        yy += 6;
                    }
                    xx += 6;
                }
                ++y;
            }
            ++x;
        }
        Frame[] framesArray = (Frame[])frames.stream().toArray(n -> new Frame[n]);
        return framesArray;
    }
}
