package de.vitox.ratolotl.client.util;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.geom.Area;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.awt.image.RenderedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.Iterator;
import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;

public final class ImageUtil {
    public static final BufferedImage EMPTY_IMAGE = new BufferedImage(1, 1, 2);

    private ImageUtil() {
    }

    public static BufferedImage loadImage(String path) {
        try {
            BufferedImage image = ImageIO.read((URL)ImageUtil.class.getResource(path));
            return image;
        }
        catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static BufferedImage loadExternalImage(File file) {
        try {
            BufferedImage image = ImageIO.read((File)file);
            return image;
        }
        catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static BufferedImage loadExternalImage(String path) {
        File file = new File(path);
        BufferedImage image = ImageUtil.loadExternalImage(file);
        return image;
    }

    public static BufferedImage scaleImage(BufferedImage image, int width, int height) {
        if (image.getWidth() == width && image.getHeight() == height) {
            return image;
        }
        int type = image.getType();
        BufferedImage newImage = new BufferedImage(width, height, type);
        Graphics2D g = newImage.createGraphics();
        ImageUtil.applyLowGraphics(g);
        g.drawImage((Image)image, 0, 0, width, height, null);
        g.dispose();
        return newImage;
    }

    public static BufferedImage scaleImage(BufferedImage image, float scale) {
        if (scale == 1.0f) {
            return image;
        }
        int width = (int)((float)image.getWidth() * scale);
        int height = (int)((float)image.getHeight() * scale);
        return ImageUtil.scaleImage(image, width, height);
    }

    public static void applyLowGraphics(Graphics2D g) {
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
        g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_SPEED);
        g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_NEAREST_NEIGHBOR);
        g.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_SPEED);
    }

    public static void applyHighGraphics(Graphics2D g) {
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
        g.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
    }

    public static byte[] toByteArray(BufferedImage image, String format) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            ImageIO.write(image, "JPG", out);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        byte[] data = out.toByteArray();
        return data;
    }

    public static byte[] toByteArray(BufferedImage image) {
        return toByteArray(image, "JPG");
    }

    public static byte[] toByteArray(BufferedImage image, float quality) {
        Iterator<ImageWriter> writers = ImageIO.getImageWritersByFormatName("JPG");
        ImageWriter writer = writers.next();
        ImageWriteParam param = writer.getDefaultWriteParam();
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        param.setCompressionMode(2);
        param.setCompressionQuality(quality);
        try {
            ImageOutputStream imageOut = ImageIO.createImageOutputStream(out);
            writer.setOutput(imageOut);
            writer.write(null, new IIOImage(image, null, null), param);
            return out.toByteArray();
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static BufferedImage toImage(byte[] data) {
        ByteArrayInputStream in = new ByteArrayInputStream(data);
        try {
            System.out.println("w");
            return ImageIO.read(in);
        }
        catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static BufferedImage convert(BufferedImage image, int type) {
        int width = image.getWidth();
        int height = image.getHeight();
        BufferedImage newImage = new BufferedImage(width, height, type);
        Graphics2D g = newImage.createGraphics();
        g.drawImage((Image)image, 0, 0, null);
        g.dispose();
        return newImage;
    }

    public static Shape getShape(BufferedImage image) {
        Area area = new Area();
        int width = image.getWidth();
        int height = image.getHeight();
        int x = 0;
        while (x < width) {
            int y = 0;
            while (y < height) {
                int argb = image.getRGB(x, y);
                int alpha = argb >> 24 & 255;
                if (alpha != 0) {
                    Rectangle rectangle = new Rectangle(x, y, 1, 1);
                    Area point = new Area((Shape)rectangle);
                    area.add(point);
                }
                ++y;
            }
            ++x;
        }
        return area;
    }
}
