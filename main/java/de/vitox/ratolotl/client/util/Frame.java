package de.vitox.ratolotl.client.util;

import java.awt.image.BufferedImage;

public class Frame {
    public final int x;
    public final int y;
    public final BufferedImage image;
    public static final Frame EMPTY = new Frame(0, 0, ImageUtil.EMPTY_IMAGE);

    public Frame(
            int x, int y, BufferedImage
            image)

    {
        this.x = x;
        this.y = y;
        this.image = image;
    }
}
