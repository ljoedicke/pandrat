package de.vitox.ratolotl.util;

import java.io.*;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

public class SendUtil {

    private DataInputStream in;
    private DataOutputStream out;

    public static SendUtil instance = new SendUtil();

    public static SendUtil getInstance() {
        return instance;
    }

    public void initIO(Socket socket) throws IOException {
        this.in = new DataInputStream(socket.getInputStream());
        this.out = new DataOutputStream(socket.getOutputStream());
    }

    public void writeMsg(String message, Socket socket) throws IOException {
        out.write(intToByteArray(message.getBytes().length));
        out.write(message.getBytes());
        out.flush();
    }

    public byte[] intToByteArray(int value) {
        return new byte[] {
                (byte)(value >>> 24),
                (byte)(value >>> 16),
                (byte)(value >>> 8),
                (byte)value};
    }

    public String readMsg(Socket socket) throws IOException {
        byte[] bytes = new byte[in.readInt()];
        in.readFully(bytes);
        return new String(bytes, StandardCharsets.US_ASCII);
    }

    public byte readByte(Socket socket) throws IOException {
        return in.readByte();
    }

    public int readInt(Socket socket) throws IOException {
        return in.readInt();
    }

    public void read(byte[] buffer, Socket socket) throws IOException {
        in.readFully(buffer);
    }

    public short readShort(Socket socket) throws IOException {
        return in.readShort();
    }

}
