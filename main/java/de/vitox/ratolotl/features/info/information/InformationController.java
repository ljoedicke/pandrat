package de.vitox.ratolotl.features.info.information;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import java.net.URL;
import java.util.ResourceBundle;

public class InformationController implements Initializable {

    @FXML
    public TableView<Information> tableInformation;

    @FXML
    public TableColumn<Information, String> colName;

    @FXML
    public TableColumn<Information, String> colInformation;

    public static ObservableList<Information> informations = FXCollections.observableArrayList();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        colName.setCellValueFactory(new PropertyValueFactory<Information, String>("name"));
        colInformation.setCellValueFactory(new PropertyValueFactory<Information, String>("information"));
        tableInformation.setItems(informations);
    }
}
