package de.vitox.ratolotl.features.info.information;

import java.net.Socket;

public class Information {

    private String name;
    private String information;
    private Socket socket;

    public Information(String name, String information, Socket socket) {
        this.name = name;
        this.information = information;
        this.socket = socket;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInformation() {
        return information;
    }

    public void setInformation(String information) {
        this.information = information;
    }

    public Socket getSocket() {
        return socket;
    }

    public void setSocket(Socket socket) {
        this.socket = socket;
    }
}
