package de.vitox.ratolotl.features.files.explorer.manager;

import javafx.scene.image.ImageView;

import java.net.Socket;
import java.util.Objects;

public class File {

    private ImageView icon;
    private String name;
    private String type;
    private String size;
    private String dateModified;
    private String path;
    private Socket socket;

    public File(ImageView icon, String name, String type, String size, String dateModified, String path, Socket socket) {
        this.icon = icon;
        this.name = name;
        this.type = type;
        this.size = size;
        this.dateModified = dateModified;
        this.path = path;
        this.socket = socket;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        File file = (File) o;
        return size == file.size &&
                Objects.equals(name, file.name) &&
                Objects.equals(type, file.type) &&
                Objects.equals(dateModified, file.dateModified);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, type, size, dateModified);
    }

    public ImageView getIcon() {
        return icon;
    }

    public void setIcon(ImageView icon) {
        this.icon = icon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getDateModified() {
        return dateModified;
    }

    public void setDateModified(String dateModified) {
        this.dateModified = dateModified;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Socket getSocket() {
        return socket;
    }

    public void setSocket(Socket socket) {
        this.socket = socket;
    }
}
