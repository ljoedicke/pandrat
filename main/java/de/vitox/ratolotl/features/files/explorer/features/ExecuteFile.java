package de.vitox.ratolotl.features.files.explorer.features;

import de.vitox.ratolotl.features.files.explorer.manager.File;
import de.vitox.ratolotl.util.SendUtil;
import javafx.event.ActionEvent;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableView;

import java.io.IOException;

public class ExecuteFile extends MenuItem {

    public ExecuteFile(TableView<File> table) {
        this.setText("Execute File");
        this.setOnAction((ActionEvent event) -> {
            try {
                SendUtil.getInstance().writeMsg("CMD_EXECUTE_FILE", table.getSelectionModel().getSelectedItem().getSocket());
                System.out.println(table.getSelectionModel().getSelectedItem().getName());
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
}
