package de.vitox.ratolotl.features.client;

import de.vitox.ratolotl.features.ITask;
import de.vitox.ratolotl.user.InfectedUser;
import de.vitox.ratolotl.util.SendUtil;
import javafx.event.ActionEvent;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableView;

import java.io.IOException;

public class UninstallClient extends MenuItem implements ITask {

    public UninstallClient(TableView<InfectedUser> table) {
        this.setText("Uninstall Client");
        this.setOnAction((ActionEvent event) -> {

            try {
               sendCommand("CMD_UNINSTALL", table);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public void sendCommand(String command, TableView<InfectedUser> table) throws IOException {
        SendUtil.getInstance().writeMsg(command, table.getSelectionModel().getSelectedItem().getSocket());
        System.out.println("Send Uninstall command!");
    }

    @Override
    public void executeTask(TableView<InfectedUser> table) throws IOException {

    }
}
