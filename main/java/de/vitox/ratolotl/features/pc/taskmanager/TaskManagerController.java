package de.vitox.ratolotl.features.pc.taskmanager;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import java.net.URL;
import java.util.ResourceBundle;

public class TaskManagerController implements Initializable {

    @FXML
    public TableView<Task> tableTaskManager;

    @FXML
    public TableColumn<Task, String> colProcess;

    public static ObservableList<Task> tasks = FXCollections.observableArrayList();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        colProcess.setCellValueFactory(new PropertyValueFactory<Task, String>("task"));
        tableTaskManager.setItems(tasks);
        createDropdown(tableTaskManager);
    }

    public void createDropdown(TableView<Task> table) {
        ContextMenu menu = new ContextMenu();

        menu.getItems().add(new KillTask(table));

        table.setContextMenu(menu);
    }

}
