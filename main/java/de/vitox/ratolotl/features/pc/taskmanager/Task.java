package de.vitox.ratolotl.features.pc.taskmanager;

import java.net.Socket;

public class Task {

    private String task;
    private Socket socket;

    public Task(String task, Socket socket) {
        this.task = task;
        this.socket = socket;
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public Socket getSocket() {
        return socket;
    }

    public void setSocket(Socket socket) {
        this.socket = socket;
    }
}
