package de.vitox.ratolotl.features.pc;

import de.vitox.ratolotl.features.ITask;
import de.vitox.ratolotl.user.InfectedUser;
import de.vitox.ratolotl.util.SendUtil;
import javafx.event.ActionEvent;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableView;

import javax.swing.*;
import java.io.IOException;
import java.net.InetAddress;

public class Shutdown extends MenuItem implements ITask {

    public Shutdown(TableView<InfectedUser> table) {
        this.setText("Shutdown");
        this.setOnAction((ActionEvent event) -> {

            try {
                InetAddress hostname = InetAddress.getLocalHost();

                if (table.getSelectionModel().getSelectedItem().getName().equals(hostname.getHostName())) {
                    int returnValue= JOptionPane.showConfirmDialog(null, "Do you really want to shutdown your own PC?", "Are you sure?", JOptionPane.YES_NO_OPTION);
                    if (returnValue == JOptionPane.YES_OPTION) {
                        sendCommand("CMD_SHUTDOWN", table);
                    }
                } else {
                    sendCommand("CMD_SHUTDOWN", table);
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public void sendCommand(String command, TableView<InfectedUser> table) throws IOException {
        SendUtil.getInstance().writeMsg(command, table.getSelectionModel().getSelectedItem().getSocket());
        System.out.println("Send Shutdown command!");
    }

    @Override
    public void executeTask(TableView<InfectedUser> table) throws IOException {

    }
}
